<?php

namespace Drupal\commerce_sofortbanking;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Psr\Log\LoggerInterface;

/**
 * Default order lock service implementation.
 */
class OrderLock implements OrderLockInterface {

  /**
   * The locking layer instance.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  protected LockBackendInterface $lock;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * Static cache of active order locks.
   *
   * @var string[]
   */
  protected array $activeLocks;

  /**
   * Constructs a new OrderLock object.
   *
   * @param \Drupal\Core\Lock\LockBackendInterface $lock
   *   The locking layer instance.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_channel_factory
   *   The logger channel factory.
   */
  public function __construct(LockBackendInterface $lock, LoggerChannelFactoryInterface $logger_channel_factory) {
    $this->lock = $lock;
    $this->logger = $logger_channel_factory->get('commerce_sofortbanking');
    $this->activeLocks = [];
  }

  /**
   * {@inheritdoc}
   */
  public function lock(OrderInterface $order, $max_lock_attempts = 1, $log_failed = FALSE): bool {
    $lock_attempts = 0;
    $lock_name = $this->getLockName($order);
    while (!$this->lock->lockMayBeAvailable($lock_name)) {
      $lock_attempts++;
      if ($lock_attempts > $max_lock_attempts) {
        return FALSE;
      }
      if ($log_failed) {
        $this->logger->info('Order ID @order_id needs wait to process, as no lock is available currently (most likely processed in parallel).', [
          '@order_id' => $order->id(),
        ]);
      }
      // Wait for webhooks (IPN) or customer return request running in parallel.
      $this->lock->wait($lock_name, 5);
    }
    $success = $this->lock->acquire($lock_name, 5);
    if ($success) {
      $this->activeLocks[$lock_name] = $lock_name;
    }
    return $success;
  }

  /**
   * {@inheritdoc}
   */
  public function release(OrderInterface $order): void {
    $lock_name = $this->getLockName($order);
    if (isset($this->activeLocks[$lock_name])) {
      $this->lock->release($lock_name);
      unset($this->activeLocks[$lock_name]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function releaseAll(): void {
    foreach ($this->activeLocks as $lock_name) {
      $this->lock->release($lock_name);
      unset($this->activeLocks[$lock_name]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getLockName(OrderInterface $order): string {
    return 'commerce_sofortbanking:' . $order->uuid();
  }

}
