<?php

namespace Drupal\commerce_sofortbanking\EventSubscriber;

use Drupal\commerce_sofortbanking\OrderLockInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\TerminateEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Event subscriber releasing active order locks on terminating the request.
 */
class OrderLockReleaseEventSubscriber implements EventSubscriberInterface {

  /**
   * The order lock service.
   *
   * @var \Drupal\commerce_sofortbanking\OrderLockInterface
   */
  protected OrderLockInterface $orderLock;

  /**
   * Constructs a new OrderLockReleaseEventSubscriber object.
   *
   * @param \Drupal\commerce_sofortbanking\OrderLockInterface $order_lock
   *   The order lock service.
   */
  public function __construct(OrderLockInterface $order_lock) {
    $this->orderLock = $order_lock;
  }

  /**
   * Releases open order locks once output flushed.
   *
   * @param \Symfony\Component\HttpKernel\Event\TerminateEvent $event
   *   The post response event.
   */
  public function releaseOrderLocks(TerminateEvent $event): void {
    $this->orderLock->releaseAll();
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      KernelEvents::TERMINATE => 'releaseOrderLocks',
    ];
  }

}
