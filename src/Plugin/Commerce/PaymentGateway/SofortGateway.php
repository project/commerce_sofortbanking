<?php

namespace Drupal\commerce_sofortbanking\Plugin\Commerce\PaymentGateway;

use Drupal\commerce\Response\NeedsRedirectException;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Exception\InvalidRequestException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PaymentStorageInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_price\Price;
use Drupal\commerce_price\RounderInterface;
use Drupal\commerce_sofortbanking\OrderLockInterface;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Sofort\SofortLib\Notification;
use Sofort\SofortLib\Refund;
use Sofort\SofortLib\Sofortueberweisung;
use Sofort\SofortLib\TransactionData;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides the SOFORT payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "sofort",
 *   label = "SOFORT",
 *   display_label = "SOFORT",
 *    forms = {
 *     "offsite-payment" = "Drupal\commerce_sofortbanking\PluginForm\SofortGatewayForm",
 *   },
 *   requires_billing_information = FALSE,
 * )
 */
class SofortGateway extends OffsitePaymentGatewayBase implements SofortGatewayInterface {

  /**
   * Version of this payment module.
   *
   * This is helpful, so the support staff can easily find out, if someone uses
   * an outdated module.
   *
   * @var string
   */
  public const SOFORT_INTERFACE_VERSION = 'drupal8_commerce_sofortbanking_2';

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected DateFormatterInterface $dateFormatter;

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * The payment storage.
   *
   * @var \Drupal\commerce_payment\PaymentStorageInterface
   */
  protected PaymentStorageInterface $paymentStorage;

  /**
   * The price rounder.
   *
   * @var \Drupal\commerce_price\RounderInterface
   */
  protected RounderInterface $rounder;

  /**
   * The order lock service.
   *
   * @var \Drupal\commerce_sofortbanking\OrderLockInterface
   */
  protected OrderLockInterface $orderLock;

  /**
   * The current route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected RouteMatchInterface $routeMatch;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->logger = $container->get('logger.factory')->get('commerce_sofortbanking');
    $instance->orderLock = $container->get('commerce_sofortbanking.order_lock');
    $storage = $container->get('entity_type.manager')->getStorage('commerce_payment');
    if ($storage instanceof PaymentStorageInterface) {
      $instance->paymentStorage = $storage;
    }
    $instance->rounder = $container->get('commerce_price.rounder');
    $instance->routeMatch = $container->get('current_route_match');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'configuration_key' => '',
      'reason_1' => 'Order {{order_id}}',
      'reason_2' => '',
      'buyer_protection' => FALSE,
      'enable_notifications' => TRUE,
      'notification_email' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['configuration_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Configuration key'),
      '#default_value' => $this->configuration['configuration_key'],
      '#required' => TRUE,
    ];

    $form['reason_1'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Reason 1'),
      '#description' => $this->t('Determines the text to be displayed in the reason field of the transfer (max. 27 characters - special characters will be replaced/deleted). The following placeholders will be replaced by specific values:  {{order_id}}                         ==> Order number {{order_date}}                    ==> Order date {{customer_id}}                  ==> End customer number {{customer_name}}          ==> End customer name  {{customer_company}}   ==> Company name of end customer {{customer_email}}          ==> Email address of end customer {{transaction_id}}              ==> SOFORT Banking transaction ID'),
      '#default_value' => $this->configuration['reason_1'],
      '#required' => TRUE,
    ];

    $form['reason_2'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Reason 2'),
      '#description' => $this->t('Determines the text to be displayed in the reason field of the transfer (max. 27 characters - special characters will be replaced/deleted). The following placeholders will be replaced by specific values:  {{order_id}}                         ==> Order number {{order_date}}                    ==> Order date {{customer_id}}                  ==> End customer number {{customer_name}}          ==> End customer name  {{customer_company}}   ==> Company name of end customer {{customer_email}}          ==> Email address of end customer {{transaction_id}}              ==> SOFORT Banking transaction ID'),
      '#default_value' => $this->configuration['reason_2'],
    ];

    $form['buyer_protection'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable buyer protection.'),
      '#default_value' => $this->configuration['buyer_protection'],
    ];

    $form['enable_notifications'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Transaction status change notifications'),
      '#default_value' => $this->configuration['enable_notifications'],
      '#description' => $this->t('Enables server-to-server transaction status change notifications. It is highly recommended to enable this setting on production sites! On the other hand, disabling this for test environments is needed, when the test environment is not publicly reachable (such as local dev environments).'),
    ];

    $form['notification_email'] = [
      '#type' => 'email',
      '#title' => $this->t('Notification e-mail'),
      '#description' => $this->t('You can enter an e-mail address to receive status notification e-mails from Sofort for every transaction.'),
      '#default_value' => $this->configuration['notification_email'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);

      $this->configuration['configuration_key'] = $values['configuration_key'];
      $this->configuration['reason_1'] = $values['reason_1'];
      $this->configuration['reason_2'] = $values['reason_2'];
      $this->configuration['buyer_protection'] = $values['buyer_protection'];
      $this->configuration['enable_notifications'] = $values['enable_notifications'];
      $this->configuration['notification_email'] = $values['notification_email'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function initializeSofortApi(PaymentInterface $payment, array $form): Sofortueberweisung {
    $sofort = new Sofortueberweisung($this->configuration['configuration_key']);

    $amount = $this->rounder->round($payment->getAmount());
    $sofort->setAmount((float) $amount->getNumber());
    $sofort->setCurrencyCode($amount->getCurrencyCode());
    $reason1 = $this->replaceReasonPlaceholders($this->configuration['reason_1'], $payment);
    $reason2 = $this->replaceReasonPlaceholders($this->configuration['reason_2'], $payment);
    $sofort->setReason($reason1, $reason2);
    $sofort->setSuccessUrl($form['#return_url']);
    $sofort->setAbortUrl($form['#cancel_url']);
    $sofort->setTimeoutUrl($form['#cancel_url']);
    if ($this->configuration['enable_notifications']) {
      $sofort->setNotificationUrl($this->getNotifyUrl()->toString());
    }
    if (!empty($this->configuration['notification_email'])) {
      $sofort->setNotificationEmail($this->configuration['notification_email']);
    }
    $sofort->setCustomerprotection(!empty($this->configuration['buyer_protection']));
    $sofort->setApiVersion('2.0');
    // Sets a version string which help SOFORT to analyze server requests.
    $sofort->setVersion(self::SOFORT_INTERFACE_VERSION);

    return $sofort;
  }

  /**
   * {@inheritdoc}
   */
  public function onNotify(Request $request): ?Response {
    parent::onNotify($request);

    $sofort_notification = new Notification();
    $transaction_id = $sofort_notification->getNotification($request->getContent());
    $payment = $this->paymentStorage->loadByRemoteId($transaction_id);
    if ($payment === NULL) {
      throw new InvalidRequestException($this->t('No transaction found for SOFORT transaction ID @transaction_id.', [
        '@transaction_id' => $transaction_id,
      ])->render());
    }
    $gateway = $payment->getPaymentGateway();
    if ($gateway === NULL) {
      throw new InvalidRequestException($this->t('Can not determine payment gateway.')->render());
    }
    $gatewayPlugin = $gateway->getPlugin();
    if (!($gatewayPlugin instanceof SofortGatewayInterface)) {
      throw new \InvalidArgumentException($this->t('Payment @payment_id is not an SOFORT payment.', [
        '@payment_id', $payment->id(),
      ])->render());
    }
    $has_lock = $this->orderLock->lock($payment->getOrder());
    if (!$has_lock) {
      throw new PaymentGatewayException($this->t('Cannot acquire lock for given order - try again later!')->render());
    }

    $this->processNotification($payment);
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function onCancel(OrderInterface $order, Request $request): void {
    parent::onCancel($order, $request);

    $sofort_gateway_data = $order->getData('sofort_gateway');
    if (empty($sofort_gateway_data['transaction_id'])) {
      throw new InvalidRequestException($this->t('Transaction ID missing for this SOFORT transaction.')->render());
    }

    $transaction_id = $sofort_gateway_data['transaction_id'];
    $payment = $this->paymentStorage->loadByRemoteId($transaction_id);
    if ($payment === NULL) {
      throw new InvalidRequestException($this->t('No transaction found for SOFORT transaction ID @transaction_id.', [
        '@transaction_id' => $transaction_id,
      ])->render());
    }

    $payment->getState()->applyTransitionById('void');
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request): void {
    parent::onReturn($order, $request);

    $sofort_gateway_data = $order->getData('sofort_gateway');
    if (empty($sofort_gateway_data['transaction_id'])) {
      throw new InvalidRequestException($this->t('Transaction ID missing for this SOFORT transaction.')->render());
    }

    $transaction_id = $sofort_gateway_data['transaction_id'];
    $payment = $this->paymentStorage->loadByRemoteId($transaction_id);
    if ($payment === NULL) {
      throw new InvalidRequestException($this->t('No transaction found for SOFORT transaction ID @transaction_id.', [
        '@transaction_id' => $transaction_id,
      ])->render());
    }
    $gateway = $payment->getPaymentGateway();
    if ($gateway === NULL) {
      throw new InvalidRequestException($this->t('Can not determine payment gateway.')->render());
    }
    $gatewayPlugin = $gateway->getPlugin();
    if (!($gatewayPlugin instanceof SofortGatewayInterface)) {
      throw new \InvalidArgumentException($this->t('Payment @payment_id is not an SOFORT payment.', [
        '@payment_id', $payment->id(),
      ])->render());
    }

    $has_lock = $this->orderLock->lock($order, 5, TRUE);
    if (!$has_lock) {
      $this->logger->warning('Cannot acquire order lock for 5 times. Skip processing payment state. This has to be done via webhooks call - payment ID: @payment_id / order ID: @order_id', [
        '@payment_id' => $payment->id(),
        '@order_id' => $order->id(),
      ]);
      return;
    }
    $this->processNotification($payment);
  }

  /**
   * Replaces the placeholders of the given reason pattern string.
   *
   * @todo decide, if we should switch to real tokens instead.
   *
   * @param string $pattern
   *   The given pattern, as configured in the gateway's settings, either for
   *   "reason 1" or "reason 2".
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment entity, which values will be used for replacing the
   *   placeholders by actual values.
   *
   * @return string
   *   String having the placeholders replaced by real order values.
   */
  protected function replaceReasonPlaceholders(string $pattern, PaymentInterface $payment): string {
    if (empty($pattern)) {
      return '';
    }
    $order = $payment->getOrder();
    if ($order === NULL) {
      // @todo Log this problem.
      return '';
    }

    $subject = str_replace([
      '{{transaction_id}}',
      '{{order_id}}',
      '{{order_date}}',
      '{{customer_id}}',
    ], [
      '-TRANSACTION-',
      $order->id(),
      $this->dateFormatter->format($order->getChangedTime(), 'short'),
      $order->getCustomerId(),
    ], $pattern);

    if ($billing_profile = $order->getBillingProfile()) {
      /** @var \Drupal\address\AddressInterface|null $billing_address */
      $billing_address = $billing_profile->hasField('address') && !$order->getBillingProfile()->get('address')->isEmpty() ? $order->getBillingProfile()->get('address')->first() : NULL;
      $customer_name = $billing_address ? sprintf($billing_address->getGivenName() . ' ' . $billing_address->getFamilyName()) : '';
      $customer_email = str_replace('@', ' ', $order->getEmail());
      $subject = str_replace([
        '{{customer_name}}',
        '{{customer_company}}',
        '{{customer_email}}',
      ], [
        $customer_name,
        $billing_address ? $billing_address->getOrganization() : '',
        $customer_email,
      ], $subject);
    }

    return $subject;
  }

  /**
   * Processes SOFORT response for both return url and async notification.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment entity.
   */
  protected function processNotification(PaymentInterface $payment): void {
    $order = $payment->getOrder();
    if ($order === NULL) {
      // @todo Log this problem.
      return;
    }
    $transaction_id = $payment->getRemoteId();
    $sofort_transaction = new TransactionData(
      $this->configuration['configuration_key']
    );
    $sofort_transaction->addTransaction($transaction_id);
    $sofort_transaction->sendRequest();
    $sofort_transaction->setApiVersion('2.0');
    if ($sofort_transaction->isError()) {
      $this->orderLock->release($order);
      throw new PaymentGatewayException($this->t('Transaction status not available for SOFORT transaction ID @transaction_id (error: @error).', [
        '@transaction_id' => $transaction_id,
        '@error' => $sofort_transaction->getError(),
      ])->render());
    }

    // We could have an outdated order, just reload it and check the states.
    // @todo Change this when #3043180 is fixed.
    /* @see https://www.drupal.org/project/commerce/issues/3043180 */
    try {
      $order_storage = $this->entityTypeManager->getStorage('commerce_order');
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException) {
      // @todo Log this exception.
      return;
    }
    /** @var \Drupal\commerce_order\Entity\OrderInterface|null $updated_order */
    $updated_order = $order_storage->loadUnchanged($order->id());
    if ($updated_order === NULL) {
      // @todo Log this exception.
      return;
    }
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $orgPayment */
    $orgPayment = $this->paymentStorage->loadUnchanged($payment->id());
    if (!($orgPayment instanceof PaymentInterface)) {
      // @todo Log this exception.
      return;
    }
    $payment = $orgPayment;

    $remote_state = $sofort_transaction->getStatus();
    $remote_state = empty($remote_state) ? 'cancel' : $remote_state;
    // If we have different states is because the payment has been validated
    // on the onNotify() callback and we need to force the redirection to the
    // next step or it the order will be placed twice.
    if ($remote_state === $payment->getRemoteState() || $payment->isCompleted() || $updated_order->getState()->getId() !== $order->getState()->getId()) {
      $this->orderLock->release($order);
      // Get the current checkout step and calculate the next step.
      $step_id = $this->routeMatch->getParameter('step');
      /** @var \Drupal\commerce_checkout\Entity\CheckoutFlowInterface $checkout_flow */
      $checkout_flow = $order->get('checkout_flow')->entity;
      $checkout_flow_plugin = $checkout_flow->getPlugin();
      $redirect_step_id = $checkout_flow_plugin->getNextStepId($step_id);

      throw new NeedsRedirectException(Url::fromRoute('commerce_checkout.form', [
        'commerce_order' => $updated_order->id(),
        'step' => $redirect_step_id,
      ])->toString());
    }

    $payment->setRemoteState($remote_state);
    switch ($remote_state) {
      case 'cancel':
      case 'loss':
        $payment->getState()->applyTransitionById('void');
        break;

      case 'pending':
        /*
         * Please note, that 'pending' means actually 'completed'. API doc:
         *
         * "Please keep in mind that if a SOFORT transaction has been
         *  successfully finished and SOFORT has received the confirmation from
         *  the buyer's bank, SOFORT reports the status
         *  "untraceable - sofort_bank_account_needed" (without Deutsche
         *  Handelsbank account) or "pending - not_credited_yet" (with Deutsche
         *  Handelsbank account). Both status messages have the equivalent
         *  meaning and represent the real-time transaction confirmation that
         *  should be processed by the merchant's online shop system."
         *
         * @see https://integration.sofort.com/integrationCenter-eng-DE/content/view/full/2513/
         */
        $payment->getState()->applyTransitionById('capture');
        break;

      case 'received':
      case 'untraceable':
        $payment->getState()->applyTransitionById('capture');
        break;

      case 'refunded':
        $transition_id = $sofort_transaction->getStatusReason() === 'compensation' ? 'partially_refund' : 'refund';
        $payment->getState()->applyTransitionById($transition_id);
        break;
    }
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function canRefundPayment(PaymentInterface $payment): bool {
    return in_array($payment->getState()->getId(), ['completed', 'partially_refunded'], TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL): void {
    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);
    // If not specified, refund the entire amount.
    $amount = $amount ?: $payment->getAmount();
    if ($amount === NULL) {
      // @todo Log this problem.
      return;
    }
    $this->assertRefundAmount($payment, $amount);

    $transaction_id = $payment->getRemoteId();
    if (empty($transaction_id)) {
      throw new InvalidRequestException('Transaction ID missing for this SOFORT transaction.');
    }

    $refund = new Refund($this->configuration['configuration_key']);
    $refund->addRefund($transaction_id, (float) $this->rounder->round($amount)->getNumber());
    // Sets a version string which help SOFORT to analyze server requests.
    $refund->setVersion(self::SOFORT_INTERFACE_VERSION);
    $refund->sendRequest();

    if ($refund->isError()) {
      // SOFORT-API didn't accept the data.
      throw new PaymentGatewayException(sprintf('SOFORT error: %s', $refund->getError()));
    }

    $old_refunded_amount = $payment->getRefundedAmount();
    if ($old_refunded_amount === NULL) {
      // @todo Log this problem.
      return;
    }
    $new_refunded_amount = $old_refunded_amount->add($amount);
    if ($new_refunded_amount->lessThan($payment->getBalance())) {
      $payment->setState('partially_refunded');
    }
    else {
      $payment->setState('refunded');
    }

    $payment->setRefundedAmount($new_refunded_amount);
    $payment->save();
  }

}
