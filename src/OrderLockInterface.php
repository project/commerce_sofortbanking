<?php

namespace Drupal\commerce_sofortbanking;

use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Defines the order lock service interface.
 */
interface OrderLockInterface {

  /**
   * Attempts to gather a lock for the given order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order entity to lock.
   * @param int $max_lock_attempts
   *   The maximum number of attempts to acquire the lock. Defaults to 1.
   * @param bool $log_failed
   *   Whether to log any lock attempt, where the lock is not available.
   *   Defaults to FALSE.
   *
   * @return bool
   *   TRUE, if a lock has been acquired. FALSE otherwise.
   */
  public function lock(OrderInterface $order, int $max_lock_attempts = 1, bool $log_failed = FALSE): bool;

  /**
   * Releases the lock for the given order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order entity.
   */
  public function release(OrderInterface $order): void;

  /**
   * Releases any open order lock of the current request.
   */
  public function releaseAll(): void;

  /**
   * Returns the lock name for the given order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return string
   *   The lock name.
   */
  public function getLockName(OrderInterface $order): string;

}
